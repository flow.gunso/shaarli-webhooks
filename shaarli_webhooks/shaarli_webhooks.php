<?php

/**
 * Plugin for Shaarli that send a webhook at specific events.
 *
 * A webhook is sent when:
 * * a link is saved
 * * a link is deleted
 *
 * The data sent over the webhook is the data given for each event by Shaarli itself, encoded in JSON and through an
 * HTTP POST request. Each event have it's own callback URL.
 *
 * @package shaarli_webhooks
 * @link gitlab.com/flow.gunso/shaarli-webhooks
 * @author <flow.gunso@gmail.com>
 * @version 1.0.0
 * @license https://opensource.org/licenses/Zlib zlib/libpng
 * @copyright 2020 <flow.gunso@gmail.com>
 */


/**
 * Base exception class for handling errors.
 *
 * Act as a proxy for exceptions used in the package. Allow for an extended message when additional information should
 * be passed over. Each exception extended from this base must set it's default_message.
 *
 * This class should not be used directly.
 *
 */
class BaseShaarliWebhookException extends Exception
{
    /**
     * @var string $default_message The default message of the exception.
     */
    protected $default_message = "";

    /**
     * BaseShaarliWebhookException constructor.
     * @param string $extended_message The extended exception message
     * @param int $code The exception code
     */
    public function __construct($extended_message = "", $code = 0)
    {
        if (!empty($extended_message)) {
            $message = $this->default_message . " | " . $extended_message;
        } else {
            $message = $this->default_message;
        }
        parent::__construct($message, $code);
    }
}


/**
 * Handle cURL initialization errors.
 */
class CurlInitFailed extends BaseShaarliWebhookException
{
    /**
     * CurlInitFailed constructor.
     * @param string $message The extended exception message
     */
    public function __construct($message = "")
    {
        $this->default_message = "Could not initialize cURL handler";
        parent::__construct($message, 1);
    }
}

/**
 * Handle cURL execution errors.
 */
class CurlExecFailed extends BaseShaarliWebhookException
{
    /**
     * CurlExecFailed constructor.
     * @param string $message The extended exception message
     */
    public function __construct($message = "")
    {
        $this->default_message = "Could not execute cURL handler";
        parent::__construct($message, 2);
    }
}

/**
 * Handle payload encoding errors.
 *
 * Wraps around JsonException.
 */
class PayloadEncodingFailed extends BaseShaarliWebhookException
{
    /**
     * PayloadEncodingFailed constructor.
     * @param string $message The extended exception message
     */
    public function __construct($message = "")
    {
        $this->default_message = "Failed to encode the payload";
        parent::__construct($message, 3);
    }
}


/**
 * Wrapper class around the cURL library to send a webhook.
 *
 * Functional core of the plugin. Initialize, prepare and execute a cURL handle as the webhook.
 */
class ShaarliWebhook
{
    /** @var string $url         The webhook callback URL */
    public $url;
    /** @var mixed $data         The raw data given by Shaarli */
    public $data;
    /** @var string $payload     The webhook payload */
    public $payload;
    /** @var string $encoding    The data encoding as the payload */
    public $encoding;
    /** @var array $header       The cURL HTTP header */
    public $header;
    /** @var array $options      The cURL options */
    public $options;
    /** @var mixed $curl_handle  The cURL handle */
    private $curl_handler;

    /**
     * Constructor of the webhook.
     *
     * All webhook is to be initialized with an url, data encoded by default in JSON and an initialized cURL handler.
     *
     * @param string $url The webhook callback URL.
     * @param mixed $data The webhook data.
     * @throws CurlInitFailed Raised when the cURL initialization fails.
     */
    public function __construct($url, $data)
    {
        if (!$this->curl_handler = curl_init()) {
            throw new CurlInitFailed();
        }

        $this->url = $url;
        $this->data = $data;
        $this->encoding = "json";
    }

    /**
     * Encode the data into the payload.
     *
     * @throws PayloadEncodingFailed Raised when the data encoding into the payload fails.
     */
    private function encodePayload()
    {
        try {
            $this->payload = json_encode($this->data, JSON_THROW_ON_ERROR | JSON_FORCE_OBJECT);
        } catch (JsonException $e) {
            throw new PayloadEncodingFailed($e->getMessage());
        }
    }

    /**
     * Make the HTTP headers.
     */
    private function makeHttpHeader()
    {
        $http_header = array();

        $content_type = 'Content-Type: application/json';
        $http_header[] = $content_type;

        $this->header = $http_header;
    }

    /** Generate the cURL options.
     *
     * Assign the encoded payload, assign the HTTP headers.
     */
    private function generateCurlOptions()
    {
        $this->encodePayload();
        $this->makeHttpHeader();
        $this->options = array
        (
            CURLOPT_URL => $this->url,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $this->payload,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => $this->header
        );
    }

    /**
     * Execute the cURL instance.
     */
    public function execute()
    {
        $this->generateCurlOptions();

        curl_setopt_array($this->curl_handler, $this->options);
        if (! $result = curl_exec($this->curl_handler)) {
            throw new CurlExecFailed(curl_errno($this->curl_handler) . ": " . curl_error($this->curl_handler));
        }
        curl_close($this->curl_handler);
        return true;
    }
}

/**
 * Utility function to match a string against a RegExp for an URL.
 *
 * @param string $string The string to match against the RegExp.
 * @return bool Returns true if the string matchs the expression, returns false otherwise.
 */
function is_string_an_url($string)
{
    $p = "/(http|https)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
    return preg_match($p, $string);
}

/**
 * Execute of the webhook.
 *
 * Initialize and execute the webhook based on a given callback URL and some data. Log to the PHP stderr if an exception
 * was raised.
 *
 * @param string $callback_url The callback URL for the webhook.
 * @param mixed $data The data to be sent over through the webhook.
 * @return bool Returns true when the webhook execution was successful, returns false otherwise.
 */
function execute_webhook($callback_url, $data)
{
    if (!empty($callback_url)) {
        try {
            $webhook = new ShaarliWebhook($callback_url, $data);
            $webhook->execute();
            return true;
        } catch (Exception $e) {
            error_log($e->getMessage());
        }
    }
    return false;
}

/**
 * Required function by Shaarli for the plugin to get initialized and loaded.
 *
 * As per Shaarli's documentation states, it could return an array of strings to informs the admnistrators of errors.
 * But the plugin does not require anything that Shaarli doesn't already ship, or no parameters have default
 * value, so it's an empty function as of now.
 */
function shaarli_webhooks_init()
{
}

/**
 * Hook function to the save_plugin_parameters Shaarli event.
 *
 * Validate the URL saved as the plugin parameters, log to the PHP stderr when validation fails.
 *
 * @param mixed $data Whatever type of data given by Shaarli.
 * @return mixed Return the $data back to Shaarli as it should.
 */
function hook_shaarli_webhooks_save_plugin_parameters($data)
{
    $parameter_names = array(
        "WEBHOOKS_SAVE_LINK_CALLBACK_URL",
        "WEBHOOKS_DELETE_LINK_CALLBACK_URL"
    );

    foreach ($parameter_names as $parameter_name) {
        $parameter = $data[$parameter_name];
        if (!empty($parameter) && !is_string_an_url($parameter)) {
            error_log("ShaarliWebhooks::".$parameter_name." is not an URL");
        }
    }

    return $data;
}

/**
 * Hook function to the save_link Shaarli event.
 *
 * Pass the callback URL associated to the save_link event and the data of that link given by Shaarly to the webhook
 * executor.
 *
 * @param mixed $data Whatever type of data given by Shaarli.
 * @param \Shaarli\ConfigManager $conf A ConfigManager instance given by Shaarli.
 * @return mixed Return the $data back to Shaarli as it should.
 */
function hook_shaarli_webhooks_save_link($data, $conf)
{
    execute_webhook($conf->get('plugins.WEBHOOKS_SAVE_LINK_CALLBACK_URL'), $data);
    return $data;
}

/**
 * Hook function to the delete_link Shaarli event.
 *
 * Pass the callback URL associated to the delete_link event and the data of that link given by Shaarly to the webhook
 * executor.
 *
 * @param mixed $data Whatever type of data given by Shaarli.
 * @param \Shaarli\ConfigManager $conf A ConfigManager instance given by Shaarli.
 * @return mixed Return the $data back to Shaarli as it should.
 */
function hook_shaarli_webhooks_delete_link($data, $conf)
{
    execute_webhook($conf->get('plugins.WEBHOOKS_DELETE_LINK_CALLBACK_URL'), $data);
    return $data;
}
