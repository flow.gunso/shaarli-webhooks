# `cvuorinen/phpdoc-markdown-public` modification

This is a modification of [cvuorinen/phpdoc-markdown-public](https://github.com/cvuorinen/phpdoc-markdown-public) templates files.

It still require and use the original package, specifically for the `TwigMarkdownAnchorLink` Twig extension.

# Changelog

## [1.0.0] - 2020/05/22
### Added
* PHP functions in [_index.md.twig_](data/templates/markdown/index.md.twig) and [_toc.md.twig_](data/templates/markdown/toc.md.twig):
    * In [_index.md.twig_](data/templates/markdown/index.md.twig) `method.md.twig` is included for function templating.
    * In [_toc.md.twig_](data/templates/markdown/toc.md.twig) `anchorLink()` is used.
* Method visibility in [_method.md.twig_](data/templates/markdown/method.md.twig) header, prefixing the method name.

### Changed
* `data/templates/markdown-public` directory have been renamed `data/templates/markdown`.
* `publicMethods(class)` function calls have been replaced with `class.methods` property calls in [_class.md.twig_](data/templates/mardown/class.md.twig) and [_toc.md.twig_](data/templates/mardown/toc.md.twig).

### Removed
* **twig-extension** `TwigClassPublicMethods` parameter in [_template.xml_](data/templates/markdown/template.xml).
* `sort_asc` filters for `project.indexes.classes` in [_index.md.twig_](data/templates/markdown/index.md.twig) and [_toc.md.twig_](data/templates/markdown/toc.md.twig).

# License

MIT