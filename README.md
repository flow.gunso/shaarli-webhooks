# Shaarli Webhooks
[How to install](https://gitlab.com/flow.gunso/shaarli-webhooks/-/wikis/installation) | 
[What's new](https://gitlab.com/flow.gunso/shaarli-webhooks/-/wikis/changelog) | 
[How does it work](https://gitlab.com/flow.gunso/shaarli-webhooks/-/wikis/api-documentation) | 
[How does the project work](https://gitlab.com/flow.gunso/shaarli-webhooks/-/wikis/workflow)

Plugin for [Shaarli](https://github.com/shaarli/shaarli) that enable user-defined callback URL, i.e. webhooks, for those specific Shaarli events:
- Link saving
- Link deletion

## Feature 
* Customizable callback URL for each event

## Contributing
Issues and merge requests are welcome.
