COMPOSER?=composer
PHP_VENDOR?=vendor
PHP_VENDOR_BIN?=$(PHP_VENDOR)/bin
PHPDOC?=$(PHP_VENDOR_BIN)/phpdoc
PHPDOC_XML=.phpdoc.xml
PHPCS?=$(PHP_VENDOR_BIN)/phpcs
PHPCS_STANDARD_XML=.phpcs.standard.xml
PHPUNIT?=$(PHP_VENDOR_BIN)/phpunit

PYTHON?=python3
PY_HOME?=.env/python
PY_BINARY?=$(PY_HOME)/bin/$(PYTHON)
PY_REQUIREMENTS=docs/templater/requirements.txt
PY?=PYTHONHOME=$(PY_HOME) $(PY_BINARY)

SHAARLI_WEBHOOKS_SRC=./shaarli_webhooks/
SHAARLI_WEBHOOKS_TESTS=./tests/

COMPOSER_ACTION?=install


env-composer:
	$(COMPOSER) $(COMPOSER_ACTION)

env-python:
	virtualenv -p $(PYTHON) $(PY_HOME)
	
env-pip:
	$(PY) -m pip install -r $(PY_REQUIREMENTS)

env: env-composer env-python env-pip


docs-api:
	$(PHPDOC) --config=$(PHPDOC_XML)

docs-repository:
	$(PY) -m docs.templater .builds/docs docs/

docs: docs-api docs-repository


phpcs:
	$(PHPCS) --standard=$(PHPCS_STANDARD_XML) $(SHAARLI_WEBHOOKS_SRC) $(SHAARLI_WEBHOOKS_TESTS)

lint: phpcs


phpunit:
	$(PHPUNIT) $(SHAARLI_WEBHOOKS_TESTS)

tests: phpunit