# Installation
The installation process is simple:

## First, in your server filesystem.
1. Download the [latest release available](https://gitlab.com/flow.gunso/shaarli-webhooks/-/releases).
2. Uncompressed the release wherever you can.
3. Copy the directory `shaarli-webhooks/` into your Shaarli plugin directory, usually `<shaarli>/plugins/`.

## Then, in your Shaarli instance.
1. Go to your Shaarli instance's _Plugins administrition_ page.
1. Activate the plugin **shaarli-webhooks**
2. Define your webhooks in the variables:
- `WEBHOOKS_SAVE_LINK_CALLBACK_URL` for when links are saved
- `WEBHOOKS_DELETE_LINK_CALLBACK_URL` for when links are deleted.
