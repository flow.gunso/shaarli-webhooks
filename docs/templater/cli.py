#!/usr/bin/env python

# Copyright (C) 2020  flow.gunso@gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

"""Click-based command line interface"""

from pathlib import Path
import os

import click
import gitlab

from .templater import Templater


@click.command()
@click.argument("output_directory", type=click.Path(file_okay=False))
@click.argument("input_path", type=click.Path(exists=True))
def entrypoint(output_directory, input_path):
    """The CLI entrypoint of the templater module.

    :param output_directory: The output directory of the render files, does not allow files.
    :type output_directory: click.Path()
    :param input_path: Input path to parse the template files from, allow a directory of a file.
    :type input_path: click.Path()
    """
    templater = Templater(Path(input_path), Path(output_directory))
    templater.render()
