#!/usr/bin/env python

# Copyright (C) 2020  flow.gunso@gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

"""Core module

Define the templater, define the templates inheriting from Jinja2' Template class.
"""

from pprint import pprint
import os

import jinja2

from .utils import get_raw_file_from_repository_root, increment_markdown_headers


class ChangelogTemplate(jinja2.Template):
    """Template class for the INSTALL.md.jinja2 template.

    Inheritance has been solved thanks to https://stackoverflow.com/a/28236682, since jinja2.Template use __new__.
    """

    def __new__(cls, source, *args):
        """Inherit the jinja2.Template."""
        return super().__new__(cls, source)

    def render(self):
        """Render the template."""
        self.get_changelog_text()
        return super().render(changelog=self.changelog)

    def get_changelog_text(self):
        """Assign the text from the CHANGELOG.md"""
        changelog = get_raw_file_from_repository_root("CHANGELOG.md")
        changelog = increment_markdown_headers(changelog)
        changelog = changelog.replace("All notable changes to this project will be documented in this file.\n\n", "")
        self.changelog = changelog

class InstallTemplate(jinja2.Template):
    """Template class for the INSTALL.md.jinja2 template.

    Inheritance has been solved thanks to https://stackoverflow.com/a/28236682, since jinja2.Template use __new__.
    """

    def __new__(cls, source, *args):
        """Inherit the jinja2.Template."""
        return super().__new__(cls, source)

    def render(self):
        """Render the template."""
        self.get_install_text()
        return super().render(install=self.install)

    def get_install_text(self):
        """Assign the text from the README.md"""
        self.install = get_raw_file_from_repository_root("INSTALL.md")

class HomeTemplate(jinja2.Template):
    """Template class for the HOME.md.jinja2 template.

    Inheritance has been solved thanks to https://stackoverflow.com/a/28236682, since jinja2.Template use __new__.
    """

    def __new__(cls, source, *args):
        """Inherit the jinja2.Template."""
        return super().__new__(cls, source)

    def render(self):
        """Render the template."""
        self.get_readme_text()
        return super().render(readme=self.readme)

    def get_readme_text(self):
        """Assign the text from the README.md"""
        readme = get_raw_file_from_repository_root("README.md")
        readme = readme.replace(
            "](https://gitlab.com/flow.gunso/shaarli-webhooks/-/wikis/",
            "]("
        )
        self.readme =  readme

    # def get_roadmap_data(self):
    #     """Assign the roadmap  information from GitLab project's milestones and issues."""
    #     roadmap = {}
    #     for milestone in self.gitlab_project_api.milestones.list(state="active"):
    #         roadmap[milestone.id] = get_milestone_information(milestone)
    #
    #         issues = {}
    #         issue_list = self.gitlab_project_api.issues.list(
    #             milestone=milestone.title,
    #             state="opened",
    #             order_by="priority",
    #             sort="desc")
    #         issue_list += self.gitlab_project_api.issues.list(
    #             milestone=milestone.title,
    #             state="closed",
    #             order_by="priority",
    #             sort="desc")
    #         for issue in issue_list:
    #             issues[issue.id] = get_issue_information(issue)
    #         roadmap[milestone.id]["issues"] = issues
    #
    #     self.roadmap = roadmap


class Templater:
    """Templater class that parse the templates files and renders them."""

    def __init__(self, input_path, output_directory):
        """Initialize the templater."""

        # Map template files to template classes.
        TEMPLATE_TO_CLASS_MAP = {
            "HOME.md.jinja2": HomeTemplate,
            "WORKFLOW.md.jinja2": jinja2.Template,
            "INSTALL.md.jinja2": InstallTemplate,
            "CHANGELOG.md.jinja2": ChangelogTemplate
        }

        # Parse the existing files in the input path.
        self.files = []
        if input_path.is_file():
            self.files.append(input_path)
        elif input_path.is_dir():
            for path in input_path.iterdir():
                if path.is_file():
                    self.files.append(path)
        else:
            raise Exception("Input was not a file nor a directory.")

        # Initialize the templates, filtering the Jinja2 templates from the parsed files.
        self.templates = {}
        for path in self.files:
            if path.suffix == ".jinja2":

                # Pick the template class for the template file from the  map.
                TemplateClass = TEMPLATE_TO_CLASS_MAP.get(path.name, None)
                if TemplateClass is None:
                    continue

                # Prepare then assign the template.
                template = {}
                template["output"] = output_directory.joinpath(path.name.replace(".jinja2", ""))
                with open(str(path), "rt") as f:
                    template_instance = TemplateClass(f.read())

                self.templates[template_instance] = template

        # Create the output directory if it does not already.
        output_directory.mkdir(parents=True, exist_ok=True)


    def render(self):
        """Render the templates."""
        for template, template_data in self.templates.items():
            with open(str(template_data["output"]), "wt") as f:
               f.write(template.render())
