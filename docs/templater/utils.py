#!/usr/bin/env python

# Copyright (C) 2020  flow.gunso@gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

"""Utilities module"""

import os
import re

from . import REPOSITORY_PATH


# def get_milestone_information(milestone):
#     """Return a dictionary of information about a milestone using a `gitlab` milestone object."""
#     d = {}
#     d["id"] = milestone.id
#     d["iid"] = milestone.iid
#     d["title"] =  milestone.title
#     d["due_date"] =  milestone.due_date
#     d["web_url"] = milestone.web_url
#     return d


# def get_issue_information(issue):
#     """Return a dictionary of information about an issue using a `gitlab` issue object."""
#     d = {}
#     d["id"] = issue.id
#     d["iid"] = issue.iid
#     d["state"] = issue.state
#     d["title"] = issue.title
#     d["labels"] = issue.labels
#     d["labels_ref"] = ["~" + l for l in issue.labels]
#     d["web_url"] = issue.web_url
#     return d

def get_raw_file_from_repository_root(filename):
    """Return the read filename from the repository path as string."""
    with open(os.path.join(REPOSITORY_PATH, filename)) as f:
        return f.read()

def increment_markdown_headers(text):
    return re.sub(r"^(#{1,5})( .*)$", r"#\1\2", text, flags=re.M)
