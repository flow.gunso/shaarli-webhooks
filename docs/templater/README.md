# Templater

This is a tiny templater associated with the parent directory.
It renders Markdown files from Jinja2 templates while fetching data from GitLab's API.

## Changelog
### 1.0.0 | 2020/05/22
* Initial release

## Licence

GPL-3.0