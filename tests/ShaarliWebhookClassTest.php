<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 5/19/20
 * Time: 6:08 PM
 */

use PHPUnit\Framework\TestCase;

final class ShaarliWebhookClassTest extends TestCase
{
    protected $url;
    protected $data;
    protected $webhook;

    protected function setUp(): void
    {
        $this->url = "webhook.site";
        $this->data = array (
            'title' => 'Webhook.site - Test, process and transform emails and HTTP requests',
            'url' => 'https://webhook.site/#!/',
        );
        $this->webhook = new ShaarliWebhook($this->url, $this->data);
    }

    public function testShaarliWebhookInitialization()
    {
        $w = new ShaarliWebhook($this->url, $this->data);
        self::assertEquals(
            $this->url,
            $w->url
        );
        self::assertEquals(
            $this->data,
            $w->data
        );
    }

    public function testInvalidUrlThrowsCurlExecFailed()
    {
        self::expectException(CurlExecFailed::class);
        $w = new ShaarliWebhook("Not an URL", $this->data);
        $w->execute();
    }

    public function testValidUrlReturnsTrue()
    {
        self::assertTrue($this->webhook->execute());
    }
}
