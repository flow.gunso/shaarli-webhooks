<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 5/19/20
 * Time: 6:06 PM
 */

use \PHPUnit\Framework\TestCase;

final class ShaarliWebhookExceptionTest extends TestCase
{

    protected $base_shaarli_webhook_exception;
    protected $curl_init_failed;
    protected $curl_exec_failed;
    protected $curl_payload_encoding_failed;
    protected $exception_with_extended_message;

    protected function setUp(): void
    {
        $this->base_shaarli_webhook_exception = new BaseShaarliWebhookException();
        $this->curl_init_failed = new CurlInitFailed();
        $this->curl_exec_failed = new CurlExecFailed();
        $this->curl_payload_encoding_failed = new PayloadEncodingFailed();
        $this->exception_with_extended_message = new CurlExecFailed("An extended reason");
    }

    public function testBaseShaarliWebhookExceptionExtendsException()
    {
        self::assertInstanceOf(
            'Exception',
            $this->base_shaarli_webhook_exception
        );
    }

    public function testExceptionsExtendsBaseShaarliWebhookException()
    {
        self::assertInstanceOf(
            'BaseShaarliWebhookException',
            $this->curl_init_failed
        );
        self::assertInstanceOf(
            'BaseShaarliWebhookException',
            $this->curl_exec_failed
        );
        self::assertInstanceOf(
            'BaseShaarliWebhookException',
            $this->curl_payload_encoding_failed
        );
    }

    public function testExceptionsCodes()
    {
        self::assertEquals(
            1,
            $this->curl_init_failed->getCode()
        );
        self::assertEquals(
            2,
            $this->curl_exec_failed->getCode()
        );
        self::assertEquals(
            3,
            $this->curl_payload_encoding_failed->getCode()
        );
    }

    public function testCurlInitFailedEDefaultMessage()
    {
        self::assertEquals(
            'Could not initialize cURL handler',
            $this->curl_init_failed->getMessage()
        );
    }

    public function testCurlExecFailedDefaultMessage()
    {
        self::assertEquals(
            'Could not execute cURL handler',
            $this->curl_exec_failed->getMessage()
        );
    }

    public function testCurlPayloadEncodingFailedDefaultMessage()
    {
        self::assertEquals(
            'Failed to encode the payload',
            $this->curl_payload_encoding_failed->getMessage()
        );
    }

    public function testShaarliWebhookExceptionExtendedMessage()
    {
        self::assertEquals(
            'Could not execute cURL handler | An extended reason',
            $this->exception_with_extended_message->getMessage()
        );
    }
}
