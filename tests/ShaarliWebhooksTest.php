<?php

use PHPUnit\Framework\TestCase;

final class DummyShaarliConfigManager
{

    protected $array;

    public function __construct()
    {
        $this->array["WEBHOOKS_SAVE_LINK_CALLBACK_URL"] = "";
        $this->array["WEBHOOKS_DELETE_LINK_CALLBACK_URL"] = "";
    }

    public function set($variable, $value)
    {
        if (key_exists($variable, $this->array)) {
            $this->array[$variable] = $value;
            return true;
        }
        return false;
    }

    public function get($variable)
    {
        if (key_exists($variable, $this->array)) {
            return $this->array[$variable];
        }
        return false;
    }
}

final class ShaarliWebhooksTest extends TestCase
{
    protected $conf;
    protected $data_link;
    protected $data_save_plugin_parameters;

    protected function setUp(): void
    {
        $this->conf = new DummyShaarliConfigManager();

        $this->data_save_plugin_parameters = array(
            "WEBHOOKS_SAVE_LINK_CALLBACK_URL" => "",
            "WEBHOOKS_DELETE_LINK_CALLBACK_URL" => ""
        );
        $this->data_link = array("Some", "data");
    }

    public function testPluginInitExists()
    {
        self::assertTrue(
            function_exists('shaarli_webhooks_init')
        );
    }

    public function testHookSaveLinkExists()
    {
        self::assertTrue(
            function_exists('hook_shaarli_webhooks_save_link')
        );
    }

    public function testHookDeleteLinkExists()
    {
        self::assertTrue(
            function_exists('hook_shaarli_webhooks_delete_link')
        );
    }

    public function testHookSavePluginParametersExists()
    {
        self::assertTrue(
            function_exists('hook_shaarli_webhooks_save_plugin_parameters')
        );
    }

    public function testPluginInitReturnsNull()
    {
        self::assertNull(
            shaarli_webhooks_init()
        );
    }

    public function testHookSaveLinkReturnsPassive()
    {
        self::assertEquals(
            $this->data_link,
            hook_shaarli_webhooks_save_link($this->data_link, $this->conf)
        );
    }

    public function testHookDeleteLinkReturnsPassive()
    {
        self::assertEquals(
            $this->data_link,
            hook_shaarli_webhooks_save_link($this->data_link, $this->conf)
        );
    }


    public function testHookSavePluginParametersReturnsPassive()
    {
        self::assertEquals(
            $this->data_save_plugin_parameters,
            hook_shaarli_webhooks_save_plugin_parameters($this->data_save_plugin_parameters)
        );
    }

    public function testExecuteWebhookReturnFalseOnEmptyUrl()
    {
        self::assertFalse(
            execute_webhook("", $this->data_link)
        );
    }

    public function testExecuteWebhookReturnsTrueOnValidUrl()
    {
        self::assertTrue(
            execute_webhook("webhook.site", $this->data_link)
        );
    }

    public function testIsStringAnUrlReturnZeroOnNonUrl()
    {
        self::assertEquals(
            0,
            is_string_an_url("This string is not an URL")
        );
    }

    public function testIsStringAnUrlReturnOneOnUrl()
    {
        self::assertEquals(
            1,
            is_string_an_url("https://some.url/")
        );
    }
}
