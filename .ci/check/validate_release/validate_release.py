#!/usr/bin/env python

# Copyright (C) 2020  flow.gunso@gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

import os
import re

from markdown import markdown
import gitlab as gl
import bs4


if __name__ == "__main__":
    # Check a milestone has been associated with the merge request.
    milestone_title = os.environ.get("CI_MERGE_REQUEST_MILESTONE", None)
    if milestone_title is None:
        raise Exception("$CI_MERGE_REQUEST_MILESTONE was not found!")

    # Check a Giltab API client is instanciable.
    bot_private_token = os.environ.get("BOT_PRIVATE_TOKEN", None)
    if bot_private_token is None:
        raise Exception("Required environment variable $BOT_PRIVATE_TOKEN was not found")
    project_id = os.environ.get("CI_PROJECT_ID", None)
    if project_id is None:
        raise Exception("Required environment variable $CI_PROJECT_ID was not found")
    gitlab = gl.Gitlab("https://gitlab.com", private_token=bot_private_token)
    project = gitlab.projects.get(project_id)

    # Check the milestone is active.
    found_milestone =  False
    for milestone in project.milestones.list(state='active'):
        if milestone.title == milestone_title:
            found_milestone = True
            break
    if not found_milestone:
        raise Exception("Milestone {} has not been found. Is it active ?".format(milestone.title))

    # Validate the milestone title against the SemVer specs.
    version = milestone.title
    semver_expression = r"(?P<major>0|[1-9]\d*)\.(?P<minor>0|[1-9]\d*)\.(?P<patch>0|[1-9]\d*)(?:-(?P<prerelease>(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+(?P<buildmetadata>[0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?"
    if re.match(semver_expression, version) is None:
        raise Exception("Milestone {} did not match the SemVer specification.".format(version))

# Look for the version in the changelog.
with open("CHANGELOG.md", "rt") as f:
    changelog_html = markdown(f.read())

found_version_in_changelog = False
soup = bs4.BeautifulSoup(changelog_html)
for version_node in soup.find_all("h2"):
    print(version_node.string)
    version_header_expression = r"\[(?P<version>.*)\] - .*"
    version_header_match = re.match(version_header_expression, str(version_node.string))
    changelog_version = version_header_match.group("version")
    if changelog_version == version:
        found_version_in_changelog = True
        break

if not found_version_in_changelog:
    raise Exception("Could not find the version {} in the changelo".format(version))
