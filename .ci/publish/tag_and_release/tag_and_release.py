#!/usr/bin/env python

# Copyright (C) 2020  flow.gunso@gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

import os
import logging
import re

import gitlab as gl

logging.basicConfig(level=logging.INFO, format="%(message)s")
logger = logging.getLogger(__name__)

class FoundMilestone(Exception):
    pass

if __name__ == "__main__":
    bot_private_token = os.environ.get("BOT_PRIVATE_TOKEN", None)
    if bot_private_token is None:
        raise Exception("Required environment variable $BOT_PRIVATE_TOKEN was not found")

    project_id = os.environ.get("CI_PROJECT_ID", None)
    if project_id is None:
        raise Exception("Required environment variable $CI_PROJECT_ID was not found")

    commit_sha = os.environ.get("CI_COMMIT_SHA", None)
    if commit_sha is None:
        raise Exception("Required environment variable $CI_COMMIT_SHA was not found.")

    # Look for a milestone in the commit parents's merge requests,
    # only when that merge request's source and target branches are master and release.
    gitlab = gl.Gitlab("https://gitlab.com", private_token=bot_private_token)
    project = gitlab.projects.get(project_id)
    commit = project.commits.get(commit_sha)
    try:
        for parent_sha in commit.parent_ids:
            parent = project.commits.get(parent_sha)
            for merge_request in parent.merge_requests():
                if merge_request["merge_commit_sha"] == commit_sha and merge_request["source_branch"] == "master" and merge_request["target_branch"] == "release":
                    raise FoundMilestone
    except FoundMilestone:
        milestone = merge_request["milestone"]
        milestone = project.milestones.get(milestone["id"])
        version = milestone.title
    else:
        raise Exception("Did not find a milestone in the commit parents's merge requests.")

    # Validate the version against the SemVer specs.
    semver_expression = r"(?P<major>0|[1-9]\d*)\.(?P<minor>0|[1-9]\d*)\.(?P<patch>0|[1-9]\d*)(?:-(?P<prerelease>(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+(?P<buildmetadata>[0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?"
    if re.match(semver_expression, version) is None:
        raise Exception("Milestone {} did not match the SemVer specification.".format(version))

    # Extract the version changes from the changelog.
    version_changes = ""
    version_header_expression = r"## \[(?P<version>.*)\]\ - .*"
    record = False
    with open("CHANGELOG.md", "r") as f:
        for line in f:
            # Try to match the current line with as a version header.
            version_header_match = re.match(version_header_expression, line)
            # If it did match.
            if version_header_match is not None:
                # And it's already recording, stop.
                if record:
                    break
                # Or the version match, start recording.
                version_match = version_header_match.group("version")
                if version_match == version:
                    record = True
            
            # Do record.
            if record:
                version_changes += line
    version_changes = version_changes.replace("# ", " ")

    # Create a tag and a release from the milestone's title.
    project.tags.create({
        "tag_name": version,
        "ref": commit_sha,
        "message": "Release v{}".format(version),
        "release_description": version_changes
    })

    # Close the milestone.
    milestone.state_event = "close"
    milestone.save()
