#!/usr/bin/env python

# Copyright (C) 2020  flow.gunso@gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

from pathlib import Path
import os
import logging

import click
import gitlab
from gitlab.exceptions import GitlabGetError

PAGES = {
    "API_DOCUMENTATION.md": {
        "slug": "api-documentation",
        "title": "API documentation"
    },
    "WORKFLOW.md": {
        "slug": "workflow",
        "title": "Workflow"
    },
    "HOME.md": {
        "slug": "home",
        "title": "Home"
    },
    "INSTALL.md": {
        "slug": "installation",
        "title": "Installation"
    },
    "CHANGELOG.md": {
        "slug": "changelog",
        "title": "Changelog"
    }
}

logging.basicConfig(level=logging.INFO, format="%(message)s")
logger = logging.getLogger(__name__)


@click.command()
@click.argument("documentations_build_path", type=click.Path(exists=True, file_okay=False))
def entrypoint(documentations_build_path):
    bot_private_token = os.environ.get("BOT_PRIVATE_TOKEN", None)
    if bot_private_token is None:
        raise Exception("Required environment variable BOT_PRIVATE_TOKEN was not found")

    gitlab_project_id = os.environ.get("CI_PROJECT_ID", None)
    if bot_private_token is None:
        raise Exception("Required environment variable CI_PROJECT_ID was not found")

    gitlab_wiki_api = gitlab.Gitlab("https://gitlab.com", private_token=bot_private_token).projects.get(gitlab_project_id).wikis
    
    documentations_build_path = Path(documentations_build_path)
    for path in documentations_build_path.iterdir():
        if path.is_file() and path.suffix == ".md":
            if path.name in PAGES:
                page_slug = PAGES[path.name]["slug"]
                page_title = PAGES[path.name]["title"]
                with open(path, "rt") as f:
                    page_content = f.read()

                try:
                    existing_page = gitlab_wiki_api.get(page_slug)
                except GitlabGetError:
                    logger.info("Creating page {}...".format(page_title))
                    gitlab_wiki_api.create({
                        'title': page_title,
                        'slug': page_slug,
                        'content': page_content
                    })
                else:
                    logger.info("Page {} already exists".format(page_title))
                    if existing_page.content != page_content:
                        logger.info("Updating page content...")
                        existing_page.content = page_content
                        existing_page.save()

if __name__ == "__main__":
    entrypoint()
